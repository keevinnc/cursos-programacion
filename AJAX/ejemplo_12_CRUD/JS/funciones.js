mostrarUsuarios();


function mostrarUsuarios() {

  //var tabla = document.getElementById('usuarios');
  var tabla=document.getElementById('usuarios').getElementsByTagName("tbody")[0];
  var peticion; //creamos peticion
  if (window.XMLHttpRequest) { //creamos el objeto dependiendo del Navegador
    peticion = new XMLHttpRequest();
  }else {
    peticion = ActiveXObject("Microsoft.XMLHTTP");}
  peticion.onreadystatechange = function() {
    if (peticion.readyState == 4 && peticion.status == 200) {
      var respuesta = JSON.parse(peticion.responseText);
      if (respuesta.length > 0) {
        for (i in respuesta) {
          var fila = tabla.insertRow(-1);
          var celda_id = fila.insertCell(0);
          var celda_email = fila.insertCell(1);
          var celda_password = fila.insertCell(2);
          var celda_nombre = fila.insertCell(3);
          var celda_apellidos = fila.insertCell(4);
          var celda_telefono = fila.insertCell(5);
          var editar = fila.insertCell(6);
          var eliminar = fila.insertCell(7);

          celda_id.innerHTML = "<b>" + respuesta[i].id +"</b>";
          celda_email.innerHTML = respuesta[i].email;
          celda_password.innerHTML = respuesta[i].password;
          celda_nombre.innerHTML = respuesta[i].nombre;
          celda_apellidos.innerHTML = respuesta[i].apellidos;
          celda_telefono.innerHTML = respuesta[i].telefono;
          editar.innerHTML = "<button type='button' class='btn btn-warning btn-sm'>Editar</button>";
          eliminar.innerHTML = "<button type='button' class='btn btn-danger btn-sm' id=" + respuesta[i].id + " onclick='eliminarUsuario(this);' >Eliminar</button>";
        }
      } else {
        var fila = tabla.insertRow(-1);
        var celda_id = fila.insertCell(0);
        celda_id.innerHTML = "<b style='color:red'>No hay registros para mostrar.</b>";
        celda_id.colSpan = 7;

      }

    }
  }

  peticion.open("GET", "mostrarUsuarios.php", "TRUE");
  peticion.send();
}

function eliminarUsuario(fila) {
  var tabla= document.getElementById("usuarios");
  // var tabla=document.getElementById('usuarios').getElementsByTagName("tbody")[0];
  var id_usuario=fila.id;
  var indice = fila.parentNode.parentNode.rowIndex;

  var respuesta = confirm("Está seguro de borrar el usuario con el id: " + id_usuario);
  var dato = "id_usuario=" + id_usuario;
  if (respuesta == true) {
    var peticion; //creamos peticion
    if (window.XMLHttpRequest) { //creamos el objeto dependiendo del Navegador
      peticion = new XMLHttpRequest();
    } else {
      peticion = ActiveXObject("Microsoft.XMLHTTP");
    }
    peticion.onreadystatechange = function() {
      if (peticion.readyState == 4 && peticion.status == 200) {
        if(peticion.responseText==="ok"){
            tabla.deleteRow(indice);
        }else{
          alert(peticion.responseText);
        }
         //mostrarUsuarios();
      }
    }
    peticion.open("POST", "borrarUsuario.php", "TRUE");
    peticion.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    peticion.send(dato);

  } else {
    alert("no se ha borrado nada");
  }
}

function nuevoUsuario(){
  var elementos_form = document.getElementById("nuevo_usuario").getElementsByTagName("input");
  var datos_usuario =recoger_datos_usuario(elementos_form);
  var peticion;
  if (window.XMLHttpRequest) { //creamos el objeto dependiendo del Navegador
    peticion = new XMLHttpRequest();
  } else {
    peticion = ActiveXObject("Microsoft.XMLHTTP");
  }


  peticion.onreadystatechange = function() {
    if (peticion.readyState == 4 && peticion.status == 200) {
      var respuesta = peticion.responseText;
      if (respuesta === "OK") {
        document.getElementById('mensaje').getElementsByTagName("h4")[0].innerHTML="Usuario creado correctamente";
        mostrarOcultarForm();
        borrarTabla();
        mostrarUsuarios();
      } else {
          document.getElementById('mensaje').innerHTML="Error al crear el Usuario.";

      }

    }
  }
  peticion.open("POST", "crearUsuario.php", "TRUE");
  peticion.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  peticion.send(datos_usuario);

}


//rrecorre todos los elementos del form y concatena el id con el valor
//para crear la estructura que se enviará al servidor
function recoger_datos_usuario(elementos) {
  var usuario_datos = "";
  for (var i = 0; i < elementos.length; i++) {
    if (i == 0) {
      usuario_datos += elementos[i].id + "=" + elementos[i].value;
    } else {
      usuario_datos += "&" + elementos[i].id + "=" + elementos[i].value;
    }
  }
  return usuario_datos;
}


//muestra el form de crear usuario

function mostrarOcultarForm(){
  var form = document.getElementById('crear_usuario');
  if(form.style.display==="none"){
    form.style.display="";
  }else{
    form.style.display="none";
  }
}


function borrarTabla(){
  var Parent = document.getElementById('usuarios').getElementsByTagName("tbody")[0];
while(Parent.hasChildNodes())
{
   Parent.removeChild(Parent.firstChild);
}
}
