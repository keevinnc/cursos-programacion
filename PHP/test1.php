<?php

// EXAMPLE DATA

$array = [
	'hello',
	'internet',
	'people'
];

/* OBJECTIVE
 * =========
 *
 * Write a php5.6 code that sorts the array $array, and then outputs the result using var_export().
 * To simplify this test, all elements of the array are ASCII strings (more precisely, only non-accented lowercase letters).
 *
 * This sort algorithm should follow 2 criteria (the 2nd one is used in case the first results in a draw)
 *  - 1st criterion is the number of vowels (aeiou) in the string. A string with more vowels should come first.
 *  - 2nd criterion is the alphabetical order of the reversed string (hello -> olleh). Order it from "a" to "z". (ASCII sort, just like the one implemented by strcmp())
 *
 * With the test data $array presented above, the expected result would be:
 *   array('people', 'internet', 'hello')
 */

// TODO: INSERT YOUR CODE HERE!
	echo "<h4>Array inicial</h4>";
	var_export($array);
	echo "<br>";



for ($i=0; $i < count($array); $i++){ //recorremos los elementos

	for ($x=$i; $x <=2; $x++) { //comparamos el elemento con los demás

		$count_uno=count_vocals($array[$i]);	//contamos el num de vocales que contienen
		$count_dos= count_vocals($array[$x]);

		if($count_uno<$count_dos){	//cambiamos la posición en caso necesario

			$aux=$array[$i];
			$array[$i]=$array[$x];
			$array[$x]=$aux;

		}elseif($count_uno==$count_dos){	// si las misma cantidad de vocacales

			$letra_palabra_uno=strrev($array[$i])[0];	//invertimos y cogemos el primer caracter
			$letra_palabra_dos=strrev($array[$x])[0];

			if(ord($letra_palabra_uno)>ord($letra_palabra_dos)){ //cambiamos la posición si es necesario
				$aux=$array[$i];
				$array[$i]=$array[$x];
				$array[$x]=$aux;
			}

		}

	}
}


echo "<h4>Array ordenado</h4>";
	var_export($array);



//función que devuelve el num de vocales
function count_vocals($palabra){
	$num_letras=0;
	for($x=0;$x<strlen($palabra);$x++){ 
		if(preg_match('/[aeiouáéíóúü]/i',$palabra[$x])){
			$num_letras+=1;
		}
	}
	return $num_letras;
}