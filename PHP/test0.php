<?php

/* Write a php 5.6 function that performs a set difference, giving two outputs.
 *
 * A and B are sorted sets of integers. This function must output {A - B} and {B - A}.
 *
 * Example
 * =======
 *
 * Input:
 * A = 1 2 3 8 9
 * B = 2 5 9 10 12 14
 *
 * Output:
 * {A - B} = 1 3 8
 * {B - A} = 5 10 12 14
 *
 * Please implement this PHP function without the help of array_*() methods, and also without in_array()
*/

// Declaramos grupos.
$grupo_a =[1, 2, 3, 8, 9];
$grupo_b = [2, 5, 9, 10, 12, 14];

//recorremos cada elemento del grupo_uno y lo comparamos con los del grupo_dos
//en el caso de que coincida con alguno pasamos la variable duplicate a true y salimos del for además no imprimimos dicho valor.

function calculate($grupo_uno, $grupo_dos){
    for ($i=0; $i < count($grupo_uno); $i++) {
      $duplicate=false;
      for ($x=0; $x < count($grupo_dos); $x++) { 
          if($grupo_uno[$i]==$grupo_dos[$x]){
            $duplicate=true;
    			  break;
         }
      }

    if (!$duplicate){ echo $grupo_uno[$i]." "; }
    }

}

//llamaos a la función
calculate($grupo_b, $grupo_a);
